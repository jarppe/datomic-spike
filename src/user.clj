(ns user
  (:require [clojure.tools.logging :as log]
            [clojure.tools.namespace.repl :as tns]
            [mount.core :as mount :refer [defstate]]
            [datomic-spike.core]))

(defn start []
  (log/info "datomic-spike: starting...")
  (mount/start)
  (log/info "datomic-spike: running"))

(defn stop []
  (log/info "datomic-spike: stopping...")
  (mount/stop)
  (log/info "datomic-spike: stopped"))

(defn go []
  (start)
  :ready)

(defn reset []
  (stop)
  (tns/refresh :after 'user/go))
