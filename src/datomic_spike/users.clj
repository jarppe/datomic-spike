(ns datomic-spike.users
  (:require [datomic.api :as d]
            [datomic-spike.core :refer [conn]]))

;;
;; Crud for user mgmt:
;;

(defn add-user! [username email roles]
  [{:db/id (d/tempid :db.part/user)
    :user/username username
    :user/email email
    :user/active? true
    :user/roles (or roles [])}])

(defn find-user-by-email! [db email]
  (d/q '[:find (pull ?e [*]) .
         :in $ ?email
         :where
         [?e :user/email ?email]
         [?e :user/active? true]]
       db
       email))

(defn add-role! [email role]
  [[:db.fn/cas [:user/email email] :user/active? true true]
   [:db/add [:user/email email] :user/roles role]])

(defn remove-role! [email role]
  [[:db.fn/cas [:user/email email] :user/active? true true]
   [:db/retract [:user/email email] :user/roles role]])

(defn delete-user! [email]
  [[:db/add [:user/email email] :user/active? false]])
