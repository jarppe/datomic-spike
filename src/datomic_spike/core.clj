(ns datomic-spike.core
  (:require [clojure.tools.logging :as log]
            [datomic.api :as d]
            [io.rkn.conformity :as conformity]
            [mount.core :refer [defstate]]))

(defn start [uri]
  (log/infof "Connecting to Datomic: uri=[%s]" uri)
  (d/create-database uri)
  (let [conn (d/connect uri)
        norms (conformity/read-resource "spike-schemas.edn")]
    (conformity/ensure-conforms conn norms [:datomic-spike/users-schema])
    conn))

(defn stop [conn]
  (log/info "Disconnecting from Datomic")
  (if conn (d/release conn))
  nil)

(defstate conn :start (start "datomic:mem://datomic-spike")
               :stop (stop conn))
