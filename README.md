# Datomic spike

Learning Datomic the usual way.

Experimenting the use of [Datomic](http://www.datomic.com) with [mount](https://github.com/tolitius/mount).
 
## TODO

* Use PostgreSQL as a data storage
* Filters, http://docs.datomic.com/filters.html
* Backup and restore

## License 

Copyright &copy; Jarppe 2016

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
