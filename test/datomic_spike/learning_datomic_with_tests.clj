(ns datomic-spike.learning-datomic-with-tests
  (:require [clojure.test :refer :all]
            [datomic.api :as d])
  (:import (java.time LocalDate)))

; Set to `false` and `conn` will be valid and open after tests are
; executed. Set to `true` to cleanup resources.
(def cleanup-after-tests? false)

;;
;; Utils:
;;

(defn long? [v]
  (or (instance? Long v)
      (instance? Long/TYPE v)))

;;
;; Schema:
;;

(defn schema! [conn]
  @(d/transact conn [{:db/id #db/id[:db.part/db]
                      :db/doc "User name"
                      :db/ident :user/username
                      :db/valueType :db.type/string
                      :db/cardinality :db.cardinality/one
                      :db.install/_attribute :db.part/db}

                     {:db/id #db/id[:db.part/db]
                      :db/doc "User email"
                      :db/ident :user/email
                      :db/valueType :db.type/string
                      :db/cardinality :db.cardinality/one
                      :db/unique :db.unique/identity
                      :db.install/_attribute :db.part/db}

                     {:db/id #db/id[:db.part/db]
                      :db/doc "User roles"
                      :db/ident :user/roles
                      :db/valueType :db.type/ref
                      :db/cardinality :db.cardinality/many
                      :db.install/_attribute :db.part/db}

                     [:db/add #db/id[:db.part/user] :db/ident :user.role/admin]
                     [:db/add #db/id[:db.part/user] :db/ident :user.role/support]
                     [:db/add #db/id[:db.part/user] :db/ident :user.role/regular]]))

;;
;; Test setup:
;;

(def conn nil)

(defn with-test-db [f]
  (let [uri (format "datomic:mem://hello-test-%s" (d/squuid))]
    (try
      (d/create-database uri)
      (let [c (d/connect uri)]
        (schema! c)
        (alter-var-root #'conn (constantly c))
        (f))
      (finally
        (when cleanup-after-tests?
          (d/release conn)
          (d/delete-database uri)
          (alter-var-root #'conn (constantly nil)))))))

(use-fixtures :each with-test-db)

;;
;; Tests:
;;

(deftest test-setup-test
  (is (instance? datomic.peer.LocalConnection conn))
  (is (instance? datomic.db.Db (d/db conn))))

(deftest initially-db-is-empty
  (is (= 0 (->> (d/db conn)
                (d/q '[:find ?e :where [?e :user/username]])
                (count)))))

(deftest insert-users-test
  ; Insert two users:
  @(d/transact conn [{:db/id #db/id[:db.part/user]
                      :user/username "foo"
                      :user/email "foo@example.com"}
                     {:db/id #db/id[:db.part/user]
                      :user/username "bar"
                      :user/email "bar@example.com"}])
  (let [db (d/db conn)]

    ; Query all antities that have :user/username attribute, check that
    ; exactly 2 are found. Result is a set of found 'rows', each row is a vector
    ; of values. Here we get just entity ID's:
    (let [result (d/q '[:find ?e :where [?e :user/username]] db)]
      ;=> #{[17592186045418] [17592186045419]}
      ; Result is a HashSet:
      (is (instance? java.util.HashSet result))
      ; Curiously, (set? result) is false, as it's not clojure.lang.IPersistentSet:
      (is (= false (set? result)))
      ; Anyhow, there are two matches:
      (is (= 2 (count result))))

    ; Query entity ids of user with email foo@example.com:
    (let [result (d/q '[:find ?e :in $ ?email :where [?e :user/email ?email]] db "foo@example.com")]
      ;=> ${[17592186045418]}
      ; We have one match:
      (is (= 1 (count result)))
      ; The match is a row:
      (is (vector? (first result)))
      ; Each row has one value, for the ?e in query
      (is (every? (comp (partial = 1) count) result)))

    ; As we know that emails are unique, we can ask datomic to return the scalar, instead
    ; or result set:
    (let [result (d/q '[:find ?e . :in $ ?email :where [?e :user/email ?email]] db "foo@example.com")]
      ;                          ^
      ;                          |-- this says give me a scalar
      ;=> 17592186045418
      (is (long? result)))

    ; Query all entities with email, including the value of email attribute:
    (let [result (d/q '[:find ?c ?n :where [?c :user/email ?n]] db)]
      ;=> #{[17592186045418 "foo@example.com"]
      ;     [17592186045419 "bar@example.com"]}
      (is (= 2 (count result)))
      (is (every? (comp long? first) result))
      (is (every? (comp string? second) result))
      (is (= #{"foo@example.com" "bar@example.com"} (->> result (map second) (set)))))

    ; Query just the usernames:
    (let [result (d/q '[:find [?n ...] :where [_ :user/username ?n]] db)]
      ;=> ["foo" "bar"]
      (is (= #{"foo" "bar"} (-> result set))))

    ; Query usernames and emails:
    (let [result (d/q '[:find ?username ?email
                        :where
                        [?e :user/username ?username]
                        [?e :user/email ?email]] db)]
      ;=> #{["foo" "foo@example.com"]
      ;     ["bar" "bar@example.com"]}
      (is (= #{["foo" "foo@example.com"]
               ["bar" "bar@example.com"]}
             result)))

    ; Query entity id, then fetch other attributes using d/entity:
    (let [entity-id (d/q '[:find ?e . :in $ ?email :where [?e :user/email ?email]] db "foo@example.com")
          entity (d/entity db entity-id)]
      (is (= "foo" (:user/username entity)))
      (is (= "foo@example.com" (:user/email entity))))))


(deftest email-is-unique-test
  ; Insert one user:
  @(d/transact conn [{:db/id #db/id[:db.part/user]
                      :user/username "foo"
                      :user/email "foo@example.com"}])
  ; Check that there is one user ["foo" "foo@example.com"]:
  (is (= #{["foo" "foo@example.com"]}
         (d/q '[:find ?username ?email
                :where
                [?e :user/username ?username]
                [?e :user/email ?email]] (d/db conn))))
  ; Insert user "bar" with existing email:
  @(d/transact conn [{:db/id #db/id[:db.part/user]
                      :user/username "bar"
                      :user/email "foo@example.com"}])
  ; Check that there is one user ["bar" "foo@example.com"]:
  (is (= #{["bar" "foo@example.com"]}
         (d/q '[:find ?username ?email
                :where
                [?e :user/username ?username]
                [?e :user/email ?email]] (d/db conn))))
  ; d/transact returns a deref that will resolve to a map with :db-after, we
  ; can use that to query the DB after tx:
  (is (= #{["baz" "foo@example.com"]}
         (->> (d/transact conn [{:db/id #db/id[:db.part/user]
                                 :user/username "baz"
                                 :user/email "foo@example.com"}])
              deref
              :db-after
              (d/q '[:find ?username ?email
                     :where
                     [?e :user/username ?username]
                     [?e :user/email ?email]])))))

(deftest user-roles-test
  ; Insert two users with some roles:
  @(d/transact conn [{:db/id #db/id[:db.part/user]
                      :user/username "foo"
                      :user/email "foo@example.com"
                      :user/roles [:user.role/admin :user.role/support :user.role/regular]}

                     {:db/id #db/id[:db.part/user]
                      :user/username "bar"
                      :user/email "bar@example.com"
                      :user/roles :user.role/regular}])
  ; Query roles of user "foo@example.com":
  (is (= #{:user.role/admin :user.role/support :user.role/regular}
         (->> (d/q '[:find (pull ?roles [:db/ident])
                     :in $ ?email
                     :where
                     [?e :user/email ?email]
                     [?e :user/roles ?roles]]
                   (d/db conn)
                   "foo@example.com")
              (map (comp :db/ident first))
              (set))))
  ; Query users with role :user.role/regular
  (is (= #{"foo@example.com" "bar@example.com"}
         (->> (d/q '[:find ?email
                     :in $ ?role
                     :where
                     [?e :user/email ?email]
                     [?e :user/roles ?role]]
                   (d/db conn)
                   :user.role/regular)
              (map first)
              (set)))))

(deftest look-ma-no-database
  (is (= #{["foo@example.com"]}
         (d/q '[:find ?email
                :in $ ?name
                :where
                [?e :user/username ?name]
                [?e :user/email ?email]]
              [[1 :user/username "foo"]
               [1 :user/email "foo@example.com"]
               [2 :user/username "bar"]
               [2 :user/email "bar@example.com"]]
              "foo"))))

(deftest users-born-since-test
  (is (= #{["bar"]} (-> (d/q '[:find ?name
                               :in $ ?since
                               :where
                               [?e :user/dob ?dob]
                               [?e :user/name ?name]
                               [(> ?dob ?since)]]
                             [[1 :user/name "foo"]
                              [1 :user/dob (LocalDate/of 2000 1 1)]
                              [2 :user/name "bar"]
                              [2 :user/dob (LocalDate/of 2000 3 1)]]
                             (LocalDate/of 2000 2 1))))))

(deftest db-functions-basics
  ; Add DB function:
  @(d/transact conn [{:db/id #db/id [:db.part/user]
                      :db/ident :say-hello
                      :db/fn (d/function {:lang 'clojure
                                          :params '[x]
                                          :code '(str "Greetings, " x "!")})}])
  ; Fetch and invoke it:
  (let [db (d/db conn)
        e (d/entity db :say-hello)
        a (:db/fn e)]
    (is (= "Greetings, earthling!"
           (.invoke a "earthling"))))
  ; Same, but fluent:
  (is (= "Greetings, earthling!"
         (-> conn
             (d/db)
             (d/entity :say-hello)
             :db/fn
             (.invoke "earthling"))))
  ; Same, but with d/invoke
  (is (= "Greetings, earthling!"
         (d/invoke (d/db conn) :say-hello "earthling"))))

(deftest db-functions-in-tx
  ; Say you want to insert new user. The tx should fail
  ; if the user already exists. Checking existence outside
  ; of tx would be a race condition.

  ; Add DB functions, one to check is email exists, and another
  ; that throws if email exists:
  @(d/transact conn [{:db/id #db/id [:db.part/user]
                      :db/ident :email-exists?
                      :db/fn (d/function
                               {:lang 'clojure
                                :params '[db email]
                                :code '(not (nil? (d/q '[:find ?e .
                                                         :in $ ?email
                                                         :where [?e :user/email ?email]]
                                                       db
                                                       email)))})}

                     {:db/id #db/id [:db.part/user]
                      :db/ident :assert-new-user
                      :db/fn (d/function
                               {:lang 'clojure
                                :params '[db email]
                                :code '(if (d/invoke db :email-exists? db email)
                                         (throw (ex-info "email-exists" {}))
                                         ; Shoudl return tx data, but nil is ok too.
                                         )})}])
  (let [add-user! (fn [username email]
                    @(d/transact conn [; Call our own tx function here:
                                       [:assert-new-user email]
                                       ; Normal add here:
                                       {:db/id #db/id[:db.part/user]
                                        :user/username username
                                        :user/email email}]))]
    ; Add user with email "foo@example.com", this should succeed:
    (add-user! "foo" "foo@example.com")

    ; Check that user was added:
    (is (= "foo" (d/q '[:find ?username .
                        :in $ ?email
                        :where
                        [?e :user/email ?email]
                        [?e :user/username ?username]]
                      (d/db conn)
                      "foo@example.com")))

    ; Now try adding it again:
    (try
      (add-user! "foo" "foo@example.com")
      ; Shoudl throw
      (is false)
      (catch java.util.concurrent.ExecutionException e
        (is (= "clojure.lang.ExceptionInfo: email-exists {}"
               (.getMessage e)))))))
