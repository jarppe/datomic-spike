(ns datomic-spike.users-test
  (:require [clojure.test :refer :all]
            [datomic.api :as d]
            [datomic-spike.core :as c]
            [datomic-spike.users :as u]
            [clojure.string :as str]))

;;
;; Test setup:
;;

(defn with-test-db [f]
  (let [uri (format "datomic:mem://hellospike-test-%s" (d/squuid))
        conn (c/start uri)]
    (with-redefs [c/conn conn]
      (f))
    (c/stop conn)))

(use-fixtures :each with-test-db)

;;
;; Tests:
;;

(deftest user-life-cycle-test
  (let [username "foo"
        email "foo@excample.com"
        initial-roles [:admin]]

    (testing "Initially there are no user with our email"
      (is (nil? (u/find-user-by-email! (d/db c/conn) email))))

    (testing "Adding user and checking it is found"
      @(d/transact c/conn (u/add-user! username email initial-roles))
      (is (= {:user/username username
              :user/email email
              :user/roles initial-roles
              :user/active? true}
             (-> (u/find-user-by-email! (d/db c/conn) email)
                 (dissoc :db/id)))))

    (testing "Add user a :support role, ans remove :admin role"
      @(d/transact c/conn (concat (u/add-role! email :support)
                                  (u/remove-role! email :admin)))
      (is (= [:support]
             (-> (u/find-user-by-email! (d/db c/conn) email)
                 :user/roles))))

    (testing "Try adding another user with same email"
      (try
        @(d/transact c/conn (u/add-user! "bar" email []))
        ; Should fail
        (is false)
        (catch java.util.concurrent.ExecutionException e
          (is (instance? java.lang.IllegalStateException (.getCause e)))
          (is (str/starts-with? (-> e .getCause .getMessage) ":db.error/unique-conflict Unique conflict: :user/email")))
        (catch Exception e
          ; Wrong exception
          (is false))))

    (testing "Make user 'deleted'"
      @(d/transact c/conn (u/delete-user! email))
      (is (nil? (u/find-user-by-email! (d/db c/conn) email))))

    (testing "Can't alter roles of deleted user"
      (try
        @(d/transact c/conn (u/add-role! email :admin))
        (is false)
        (catch java.util.concurrent.ExecutionException e
          (is (instance? java.lang.IllegalStateException (.getCause e)))
          (is (= (-> e .getCause .getMessage) ":db.error/cas-failed Compare failed: true false")))))))
