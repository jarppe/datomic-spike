(defproject datomic-spike "0.1.0-SNAPSHOT"
  :repositories {"my.datomic.com" {:url "https://my.datomic.com/repo"}}
  :dependencies [[org.clojure/clojure "1.9.0-alpha10"]
                 [org.clojure/tools.namespace "0.2.11"]
                 [mount "0.1.10"]
                 [metosin/lokit "0.1.0"]
                 [com.datomic/datomic-pro "0.9.5390" :exclusions [org.slf4j/slf4j-nop]]
                 [io.rkn/conformity "0.4.0"]])
